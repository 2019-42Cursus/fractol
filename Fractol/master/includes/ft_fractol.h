/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 20:18:40 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/07 05:43:43 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FRACTOL_H
# define FT_FRACTOL_H

# include "libft.h"
# include "mlx.h"
# include <math.h>

# define H	800
# define W	800

enum e_fractal_set {
	MANDELBROT,
	JULIA,
	BUDDHABROT,
	BURNING_SHIP
};

typedef struct s_index
{
	int		x;
	int		y;
	int		i;
}			t_index;

typedef struct s_color
{
	unsigned int	r;
	unsigned int	g;
	unsigned int	b;
}				t_color;

typedef struct s_params
{
	double	x1;
	double	x2;
	double	y1;
	double	y2;
	int		max_iteration;
}				t_params;

typedef struct s_math
{
	double	c_r;
	double	c_i;
	double	z_r;
	double	z_i;
	double	tmp;
}				t_math;

typedef struct s_image
{
	void	*img;
	char	*addr;
	int		x;
	int		y;
	int		i;
	int		pixel_color;
	int		bpp;
	int		line_len;
	int		endian;
	double	zoom_x;
	double	zoom_y;
}				t_image;

typedef struct s_mlx
{
	void	*mlx;
	void	*win;
}				t_mlx;

typedef struct s_data
{
	t_mlx				mlx;
	enum e_fractal_set	set;
	t_image				image;
	t_index				index;
	t_params			params;
	t_math				math;

}				t_data;

int		init_fractol(t_data *data);
int		init_img(t_data *data);
void	draw_fractol(t_data *data, int init);
int		end_fractol(t_data *data);

void	put_pixel_in_img(t_data *d, int x, int y, int color);

void	setup_hook(t_data *data);
int		keypress(int keycode, t_data *datafractol);
int		zoom(int keycode, t_data *data);

void	exit_usage(void);
void	exit_init(t_data *data);
void	exit_fractol(void);

#endif