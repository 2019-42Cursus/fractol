/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 20:19:16 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/07 05:47:06 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

/**
** @name draw_fractol();
** @brief Call by main function, this function call and draw the fractal set
** compared to fractol->set defined earlier. The Param init is for use default
** init or not.
**
** @param t_fractol *fractol
** @param int init
**/
// void
// 	draw_fractol(t_fractol *fractol, int init)
// {
// 	if (fractol->set == MANDELBROT)
// 		mandelbrot(fractol, init);
// 	else if (fractol->set == JULIA)
// 		julia(fractol);
// 	else if (fractol->set == BUDDHABROT)
// 		buddhabrot(fractol);
// 	mlx_put_image_to_window(fractol->mlx.mlx, fractol->mlx.win, fractol->img.img, 0, 0);
// }

/**
** @name init_fractol_set(); [Static Function]
** @brief Call by main function, this function init the fractol set
** among a predefined enum class call e_fractal_set{}.
**
** @param char *set
** @param t_data *data
** @return TRUE [1]
** @return FALSE [0]
**/
static int
	init_fractol_set(char *set, t_data *data)
{
	if (!ft_strcmp(set, "Mandelbrot") || !ft_strcmp(set, "M"))
		data->set = MANDELBROT;
	else if (!ft_strcmp(set, "Julia") || !ft_strcmp(set, "J"))
		data->set = JULIA;
	else if (!ft_strcmp(set, "Buddhabrot") || !ft_strcmp(set, "B"))
		data->set = BUDDHABROT;
	else if (!ft_strcmp(set, "Burning_ship") || !ft_strcmp(set, "BS"))
		data->set = BURNING_SHIP;
	else
		return (FALSE);
	return (TRUE);
}

/**
** @name main();
** @brief This function is the main function.
**
** @param int argc
** @param char *argv[]
**/
int
	main(int argc, char *argv[])
{
	t_data	data;

	if (argc != 2 || !init_fractol_set(argv[1], &data))
		exit_usage();
	if (!init_fractol(&data) || !init_img(&data))
		exit_init(&data);
	// draw_fractol(&fractol, TRUE);
	mlx_loop(data.mlx.mlx);
	end_fractol(&data);
	return (0);
}
