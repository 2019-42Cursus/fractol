/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_fractol_init.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 05:33:14 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/07 05:47:42 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

/**
** @name init_img()
** @brief Call by main function, this function init the img used
** to render fractol set later.
**
** @param t_data *data
** @return TRUE [1]
** @return FALSE [0]
**/
int
	init_img(t_data *data)
{
	data->image.img = mlx_new_image(data->mlx.mlx, W, H);
	if (!data->image.img)
		return (FALSE);
	data->image.addr = mlx_get_data_addr(data->image.img, &data->image.bpp,
		&data->image.line_len, &data->image.endian);
	if (!data->image.addr)
		return (FALSE);
	return (TRUE);
}

/**
** @name init_fractol()
** @brief Call by main function, this function init mlx lib
** and all dependencies
**
** @param t_data *data
** @return TRUE [1]
** @return FALSE [0]
**/
int
	init_fractol(t_data *data)
{
	data->mlx.mlx = mlx_init();
	if (!data->mlx.mlx)
		return (FALSE);
	data->mlx.win = mlx_new_window(data->mlx.mlx, H, W, "Fractol");
	if (!data->mlx.win)
		return (FALSE);
	setup_hook(data);
	return (TRUE);
}