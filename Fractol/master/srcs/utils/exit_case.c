/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit_case.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 00:09:37 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/07 05:46:39 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

/**
** @name exit_usage();
** @brief Call when usage is incorrect, exit after print error.
**/
void
	exit_usage(void)
{
	ft_putstr("\n\t\t\t\e[1;31m! [INVALID USAGE] !\e[0m\n");
	ft_putstr("\n\t\e[0;36mCorrect usage is	: ");
	ft_putstr("\e[0;92m./fractol <FractalSet>\e[0m\n");
	ft_putstr("\t\e[0;36mExemple\t\t\t: ");
	ft_putstr("\e[0;92m./fractol Mandelbrot\e[0m\n");
	ft_putstr("\t\e[0;36mFractol Set\t\t: ");
	ft_putstr("\e[0;92m[Mandelbrot, Julia, Buddhabrot, Burning_ship]\e[0m\n");
	exit(1);
}


/**
** @name exit_init();
** @brief Call when initialisation failed.
**
** @param t_data *data
**/
void
	exit_init(t_data *data)
{
	ft_putstr("\e[1;31mError during Initialisation !\e[0m\n");
	end_fractol(data);
	(void)data;
	exit(1);
}


/**
** @name exit_fractol();
** @brief Call when user press predefined exit key. Exit the program.
**/
void
	exit_fractol(void)
{
	ft_putstr("Fractol End\n");
	exit(0);
}
