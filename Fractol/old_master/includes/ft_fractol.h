/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fractol.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 20:18:40 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/07 02:29:28 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FRACTOL_H
# define FT_FRACTOL_H

# include "libft.h"
# include "mlx.h"
# include <math.h>

# define H	800
# define W	800

enum e_fractal_set {
	MANDELBROT,
	JULIA,
	BUDDHABROT,
	BURNING_SHIP
};

typedef struct s_index
{
	int		x;
	int		y;
	int		i;
}			t_index;

typedef struct s_color
{
	unsigned int	r;
	unsigned int	g;
	unsigned int	b;
}				t_color;

typedef struct s_data
{
	double	x1;
	double	x2;
	double	y1;
	double	y2;
	int		max_iteration;
}				t_data;

typedef struct s_math
{
	double	c_r;
	double	c_i;
	double	z_r;
	double	z_i;
	double	tmp;
}				t_math;

typedef struct s_img
{
	void	*img;
	char	*addr;
	int		x;
	int		y;
	int		i;
	int		pixel_color;
	int		bpp;
	int		line_len;
	int		endian;
	double	zoom_x;
	double	zoom_y;
}				t_img;

typedef struct s_mlx
{
	void	*mlx;
	void	*win;
}				t_mlx;

typedef struct s_fractol
{
	t_mlx				mlx;
	enum e_fractal_set	set;
	t_img				img;
	t_index				index;
	t_data				data;
	t_math				math;

}				t_fractol;

int		init_fractol(t_fractol *fractol);
int		init_img(t_fractol *fractol);
void	draw_fractol(t_fractol *fractol, int init);
int		end_fractol(t_fractol *fractol);

void	put_pixel_in_img(t_fractol *f, int x, int y, int color);

void	setup_hook(t_fractol *fractol);
int		keypress(int keycode, t_fractol *fractol);
int		zoom(int keycode, t_fractol *fractol);

void	mandelbrot(t_fractol *fractol, int init);
void	draw_mandelbrot(t_fractol *fractol);
void	julia(t_fractol *fractol);
void	buddhabrot(t_fractol *fractol);

void	exit_usage(void);
void	exit_init(t_fractol *fractol);
void	exit_fractol(void);

#endif