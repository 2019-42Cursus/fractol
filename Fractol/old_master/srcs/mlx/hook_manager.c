/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook_manager.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/11 05:28:48 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/07 02:32:04 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"
#include <limits.h>

/**
** @name setup_hook();
** @brief Call by init_fractol() function. This function init all hook event.
**
** @param t_fractol *fractol
**/
void
	setup_hook(t_fractol *fractol)
{
	mlx_key_hook(fractol->mlx.win, keypress, fractol);
	mlx_hook(fractol->mlx.win, 33, 1L << 17, end_fractol, fractol);
	mlx_mouse_hook(fractol->mlx.win, &zoom, fractol);
}

/**
** @name keypress();
** @brief Call when key is pressed. This function is init
** by mlx_key_hook() from MLX Lib in setup_hook() function.
**
** @param int keycode
** @param t_fractol *fractol
** @return keycode
**/
int
	keypress(int keycode, t_fractol *fractol)
{
	printf("key code : %d\n", keycode);
	if (keycode == 65307)
		end_fractol(fractol);
	return (keycode);
}

int
	zoom(int keycode, t_fractol *fractol)
{
	printf("key code : %d\n", keycode);
	if (keycode == 4)
	{
		
	}
	else if (keycode == 5)
	{
		
	}
	draw_mandelbrot(fractol);
	mlx_put_image_to_window(fractol->mlx.mlx, fractol->mlx.win, fractol->img.img, 0, 0);
	return (1);
}
