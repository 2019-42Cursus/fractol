/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_manager.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/23 18:53:02 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/02 03:50:01 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

/**
** @name put_pixel_in_img();
** @brief This function put colored pixel to a img.
** Shifting (x * f->img.bpp >> 3) is a binary
** operation equivalent to a division by 8,
** but shifting is more faster.
**
** @param t_fractol *fractol
** @param t_fractol *x
** @param t_fractol *y
** @param t_fractol *color
**/
void
	put_pixel_in_img(t_fractol *f, int x, int y, int color)
{
	if (x >= 0 && y >= 0 && x < W && y < H)
		*(int *)&f->img.addr[(x * f->img.bpp >> 3) +
			(y * f->img.line_len)] = color;
}