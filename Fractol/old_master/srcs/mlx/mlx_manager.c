/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_manager.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 00:06:32 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/02 03:55:57 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

/**
** @name init_img()
** @brief Call by main function, this function init the img used
** to render fractol set later.
**
** @param t_fractol *fractol
** @return TRUE [1]
** @return FALSE [0]
**/
int
	init_img(t_fractol *fractol)
{
	fractol->img.img = mlx_new_image(fractol->mlx.mlx, W, H);
	if (!fractol->img.img)
		return (FALSE);
	fractol->img.addr = mlx_get_data_addr(fractol->img.img, &fractol->img.bpp,
		&fractol->img.line_len, &fractol->img.endian);
	if (!fractol->img.addr)
		return (FALSE);
	return (TRUE);
}

/**
** @name init_fractol()
** @brief Call by main function, this function init mlx lib
** and all dependencies
**
** @param t_fractol *fractol
** @return TRUE [1]
** @return FALSE [0]
**/
int
	init_fractol(t_fractol *fractol)
{
	fractol->mlx.mlx = mlx_init();
	if (!fractol->mlx.mlx)
		return (FALSE);
	fractol->mlx.win = mlx_new_window(fractol->mlx.mlx, H, W, "Fractol");
	if (!fractol->mlx.win)
		return (FALSE);
	setup_hook(fractol);
	return (TRUE);
}

/**
** @name init_fractol()
** @brief this function is called at the very end of the program. His goal
** is to free all allocated memory.
**
** @param t_fractol *fractol
** @return TRUE [1]
** @return FALSE [0]
**/
int
	end_fractol(t_fractol *fractol)
{
	if (fractol->mlx.mlx)
	{
		if (fractol->mlx.win)
			mlx_destroy_window(fractol->mlx.mlx, fractol->mlx.win);
		if (fractol->img.img)
			mlx_destroy_image(fractol->mlx.mlx, fractol->img.img);
		mlx_loop_end(fractol->mlx.mlx);
		mlx_destroy_display(fractol->mlx.mlx);
	}
	exit_fractol();
	return (0);
}
