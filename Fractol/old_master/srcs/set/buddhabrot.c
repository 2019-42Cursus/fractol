/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buddhabrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/11 09:17:44 by thzeribi          #+#    #+#             */
/*   Updated: 2021/11/23 18:55:05 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

static void
	test(t_fractol *fractol)
{
	double x1 = -1;
	double x2 = 1;
	double y1 = -1.2;
	double y2 = 1.2;
	double iteration_max = 1000;
	double image_x = W;
	double image_y = H;

	double zoom_x = image_x/(x2 - x1);
	double zoom_y = image_y/(y2 - y1);


	for (double x = 0; x < image_x; x++)
	{
		for (double y = 0; y < image_y ; y++)
		{
			double c_r = x / zoom_x + x1;
			double c_i = y / zoom_y + y1;
			double z_r = 0;
			double z_i = 0;
			double i = 0;
			do
			{
				double tmp = z_r;
				z_r = z_r*z_r - z_i*z_i + c_r;
				z_i = 2*z_i*tmp + c_i;
				i++;
			} while (z_r*z_r + z_i*z_i < 4 && i < iteration_max);
			if (i == iteration_max)
				put_pixel_in_img(fractol, x, y, 0xFFFFFF);
		}
	}
}

/*
double x1 = -2.1;
double x2 = 0.6;
double y1 = -1.2;
double y2 = 1.2;
double zoom = 100;
double iteration_max = 100;

double image_x = W;
double image_y = H;

double zoom_x = image_x/(x2 - x1);
double zoom_y = image_y/(y2 - y1);

// un tableau que l'on va incrémenter à chaque fois que la suite Z_n passera par un point.
// définir pixels comme un tableau 2D de image_x cases sur image_y cases avec toutes les cases initialisés à 0

// en théorie, on devrait faire une seul boucle dans laquelle on devrait prendre les coordonnés (x; y) au hasard.
for (double x = 0; x < image_x; x++)
{
	for (double y = 0; y < image_y ; y++)
	{
		double c_r = x / zoom_x + x1;
		double c_i = y / zoom_y + y1;
		double z_r = 0;
		double z_i = 0;
		double i = 0;
		do
		{
			double tmp = z_r;
			z_r = z_r*z_r - z_i*z_i + c_r;
			z_i = 2*z_i*tmp + c_i
			i++;
			// ajouter les coordonnées ((z_r-x1)*zoom; (z_i-y1)*zoom) au tableau tmp_pixels
		} while (z_r*z_r + z_i*z_i < 4 && i < iteration_max);
		if (i != iteration_max)
		{
			
		}
	}
}
		do
		{
			double tmp = z_r;
			z_r = z_r*z_r - z_i*z_i + c_r;
			z_i = 2*z_i*tmp + c_i
			i++;
			// ajouter les coordonnées ((z_r-x1)*zoom; (z_i-y1)*zoom) au tableau tmp_pixels
		} while (z_r*z_r + z_i*z_i < 4 && i < iteration_max);
		if (i != iteration_max)
		{
			
		}

        si i != iteration_max
            Pour chaque valeurs pixel de tmp_pixels
                si la case pixels[pixel[0]][pixel[1]] existe
                    on incrémente la case en question
                finSi
            finPour
        finSi
    finPour
finPour

Pour chaque case de coordonnée (x; y) de l'image
    Dessiner le pixel de coordonnée (x; y) avec la couleur rgb(min(pixels[x][y], 255), min(pixels[x][y], 255), min(pixels[x][y], 255))
finPour
*/

void
	buddhabrot(t_fractol *fractol)
{
	test(fractol);
}