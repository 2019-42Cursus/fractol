/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/11 07:45:46 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/02 05:07:45 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

/**
**
** @brief This function generate the img of mandelbrot set fractal.
** @param t_tab *fractol
** TODO: Use struc for local variable + add color
** NORM: Norm function
**
** Code for algorithm based from Pseudo Code
** Source : http://sdz.tdct.org/sdz/dessiner-la-fractale-de-mandelbrot.html
**/

static void
	test(t_fractol *fractol)
{
	double x1 = -1;
	double x2 = 1;
	double y1 = -1.2;
	double y2 = 1.2;
	double iteration_max = 150;
	double image_x = W;
	double image_y = H;

	double zoom_x = image_x/(x2 - x1);
	double zoom_y = image_y/(y2 - y1);

	for (double x = 0; x < image_x; x++)
	{
		for (double y = 0; y < image_y ; y++)
		{
			double c_r = 0.285;
			double c_i = 0.01;
			double z_r = x / zoom_x + x1;
			double z_i = y / zoom_y + y1;
			double i =  0;
			do {
				double tmp = z_r;
				z_r = z_r*z_r - z_i*z_i + c_r;
				z_i = 2*z_i*tmp + c_i;
				i++;
			} while (z_r*z_r + z_i*z_i < 4 && i < iteration_max);
			if (i == iteration_max)
				put_pixel_in_img(fractol, x, y, 0xFFFFFF);
			else
				put_pixel_in_img(fractol, x, y, 0x000000);
		}
	}
}

void
	julia(t_fractol *fractol)
{
	test(fractol);
}