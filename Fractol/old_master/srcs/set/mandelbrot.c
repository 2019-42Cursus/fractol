/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 03:05:29 by thzeribi          #+#    #+#             */
/*   Updated: 2021/12/07 02:33:00 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_fractol.h"

/**
** @name init_mandelbrot(); [Static Function]
** @brief This function init the mandelbrot set variable.
** @param t_tab *fractol
**/
static void
	init_mandelbrot(t_fractol *f)
{
	f->data.x1 = -2.1;
	f->data.x2 = 0.6;
	f->data.y1 = -1.2;
	f->data.y2 = 1.2;
	f->data.max_iteration = 1000;
	f->img.x = W;
	f->img.y = H;
	f->img.zoom_x = f->img.x / (f->data.x2 - f->data.x1);
	f->img.zoom_y = f->img.y / (f->data.y2 - f->data.y1);
}

/**
** @name draw_mandelbrot();
** @brief This function generate the img of mandelbrot set fractal.
** @param t_tab *fractol
** TODO: Add color
** NORM: Norm function
**
** Code for algorithm based from Pseudo Code
** Source : http://sdz.tdct.org/sdz/dessiner-la-fractale-de-mandelbrot.html
**/
void
	draw_mandelbrot(t_fractol *f)
{
	f->index.x = 0;
	while (f->index.x < f->img.x)
	{
		f->index.y = 0;
		while (f->index.y < f->img.y)
		{
			f->math.c_r = f->index.x / f->img.zoom_x + f->data.x1;
			f->math.c_i = f->index.y / f->img.zoom_y + f->data.y1;
			f->math.z_r = 0;
			f->math.z_i = 0;
			f->img.i = 0;
			while (f->math.z_r * f->math.z_r + f->math.z_i * f->math.z_i < 4 && f->img.i < f->data.max_iteration)
			{
				f->math.tmp = f->math.z_r;
				f->math.z_r = f->math.z_r * f->math.z_r - f->math.z_i * f->math.z_i + f->math.c_r;
				f->math.z_i = 2 * f->math.z_i * f->math.tmp + f->math.c_i;
				f->img.i++;
			}
			if (f->img.i == f->data.max_iteration)
				put_pixel_in_img(f, f->index.x, f->index.y, 0xFFFFFF);
			else
				put_pixel_in_img(f, f->index.x, f->index.y, 0x000000);
			f->index.y++;
		}
		f->index.x++;
	}
}

/**
** @name mandelbrot();
** @brief This function init and draw mandelbrot set.
** @param t_tab *fractol
**/
void
	mandelbrot(t_fractol *fractol, int init)
{
	if (init)
		init_mandelbrot(fractol);
	draw_mandelbrot(fractol);
}
